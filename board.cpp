#include "pch.h"
#include "board.h"

bool operator==(const board & B, square test)
{
	return B.Board[test.row][test.col] == 0;
}
