// Declarations and functions for project #4

#include <iostream>
#include <limits.h>
#include "matrix.h"
#include <vector>
#include "d_except.h"
#include <list>
#include <fstream>
#include <stack>

using namespace std;

typedef int ValueType; // The type of the value in a cell
const int Blank = -1;  // Indicates that a cell is blank

const int SquareSize = 3;  //  The number of cells in a small square
//  (usually 3).  The board has
//  SquareSize^2 rows and SquareSize^2
//  columns.

const int BoardSize = SquareSize * SquareSize; // board size is 9x9

const int MinValue = 1;
const int MaxValue = 9;
int backtrack_count = 0;

int numSolutions = 0;

class board
	// Stores the entire Sudoku board
{
public:
	board(int);
	void clear();
	void initialize(ifstream &fin); // will read data from text file and place it into a matrix
	void print(); // prints the board
	bool isBlank(int, int); // checks if the board is blank
	ValueType getCell(int, int);
	void setCell(int i, int j, int val) { value[i][j] = val; }; // sets the value of value[i][j]
	void resetCell(int i, int j) { value[i][j] = Blank; backtrack_count++; };
	void setConflicts(); // sets conflicts in relation to rows and columns
	void setSqConflict(); // sets conflicts in relation to squares
	void printConflicts(); // prints the conflicts
	bool fullySolved(); // checks to see if the puzzle has been solved
	bool check(int i, int j, int value);
	void solve();
	void findBlankCell(int &row, int &col);
	void updateConflicts();
	void find_compatible(int i, int j, stack<int> &);
private:

	// The following matrices go from 1 to BoardSize in each
	// dimension, i.e., they are each (BoardSize+1) * (BoardSize+1)

	matrix<ValueType> value; // this is the actual sodoku puzzle
	matrix <bool> checkConflictRow; // conflicts of rows
	matrix <bool> checkConflictCol; // conflicts of columns
	matrix <bool> checkConflictSq; // conflicts of squares
};

board::board(int sqSize) : value(BoardSize + 1, BoardSize + 1), checkConflictSq(BoardSize + 1, BoardSize + 1), checkConflictRow(BoardSize + 1, BoardSize + 1), checkConflictCol(BoardSize + 1, BoardSize + 1)// Board constructor
{
	/*

	 This is a contructor for the board, its sets the board size to the parameter that is sent
	 param sqSize: size of the matrix is set to (n+1) x (n+1)

	 */
	clear(); // clear the board
}

void board::clear()
// Mark all possible values as legal for each board entry
{
	for (int i = 1; i <= BoardSize; i++)
		for (int j = 1; j <= BoardSize; j++)
		{
			value[i][j] = Blank;
		}
}

void board::setConflicts() {
	/*

	 This function sets the conflicts for rows and columns

	 */
	for (int i = 1; i < MaxValue + 1; i++) { // iterate through first loop
		for (int j = 1; j < MaxValue + 1; j++) { //iterate through second loop
			if (value[i][j] != -1) { // if there is a value in the cell
				checkConflictRow[i][value[i][j]] = true; // turn that value true (this is for rows)
			}
			if (value[j][i] != -1)
				checkConflictCol[i][value[j][i]] = true; // turn that value true (this is for columns)
		}
	}
}

void board::setSqConflict()
{
	/*

	 This function sets the conflicts for squares

	 */
	int count = 1; // counts the square we are in
	for (int a = 1; a <= MaxValue; a++) { // iterate through rows
		for (int b = 1; b <= MaxValue; b++) { // iterate through columns
			if (((a - 1) % 3 == 0) && ((b - 1) % 3 == 0)) { // if this number is divisible by 3 evenly
				for (int i = a; i < a + 3; i++) { // iterate trough a + all other spaces in that specific square
					for (int j = b; j < b + 3; j++) { // iterate trough b + all other spaces in that specific square
						if (value[i][j] != -1) { // it there is an actual value there
							checkConflictSq[count][value[i][j]] = true; // the that value in the matrix to true
						}
					}
				}
				count++; // add one to the square to edit the checkConflictSq in order
			}
		}
	}
}


void board::printConflicts()
{
	/*

	 This funtion prints all the conflicts, all loops iterate through i and j printing out every cell

	 */
	cout << "Square Conflicts.." << endl;
	for (int i = 1; i < MaxValue + 1; i++) {
		for (int j = 1; j < MaxValue + 1; j++) {
			cout << checkConflictSq[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl << "Row conflicts" << endl;
	for (int i = 1; i < MaxValue + 1; i++) {
		for (int j = 1; j < MaxValue + 1; j++) {
			cout << checkConflictRow[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl << "Column conflicts" << endl;
	for (int i = 1; i < MaxValue + 1; i++) {
		for (int j = 1; j < MaxValue + 1; j++) {
			cout << checkConflictCol[i][j] << " ";
		}
		cout << endl;
	}
}

void board::initialize(ifstream &fin)
// Read a Sudoku board from the input file.
{
	char ch;
	clear();
	for (int i = 1; i <= BoardSize; i++)
		for (int j = 1; j <= BoardSize; j++)
		{
			fin >> ch;
			// If the read char is not Blank
			if (ch != '.')
				setCell(i, j, ch - '0');   // Convert char to int
		}
}

int squareNumber(int i, int j)
// Return the square number of cell i,j (counting from left to right,
// top to bottom.  Note that i and j each go from 1 to BoardSize
{
	// Note that (int) i/SquareSize and (int) j/SquareSize are the x-y
	// coordinates of the square that i,j is in.

	return SquareSize * ((i - 1) / SquareSize) + (j - 1) / SquareSize + 1;
}

ostream &operator<<(ostream &ostr, vector<int> &v)
// Overloaded output operator for vector class.
{
	for (int i = 0; i < v.size(); i++)
		ostr << v[i] << " ";
	cout << endl;
	return ostr;
}

ValueType board::getCell(int i, int j)
// Returns the value stored in a cell.  Throws an exception
// if bad values are passed.
{
	if (i >= 1 && i <= BoardSize && j >= 1 && j <= BoardSize)
		return value[i][j];
	else
		throw rangeError("bad value in getCell");
}

bool board::isBlank(int i, int j)
// Returns true if cell i,j is blank, and false otherwise.
{
	if (i < 1 || i > BoardSize || j < 1 || j > BoardSize)
		throw rangeError("bad value in setCell");

	return (getCell(i, j) == Blank);
}

void board::print()
// Prints the current board.
{
	for (int i = 1; i <= BoardSize; i++)
	{
		if ((i - 1) % SquareSize == 0)
		{
			cout << " -";
			for (int j = 1; j <= BoardSize; j++)
				cout << "---";
			cout << "-";
			cout << endl;
		}
		for (int j = 1; j <= BoardSize; j++)
		{
			if ((j - 1) % SquareSize == 0)
				cout << "|";
			if (!isBlank(i, j))
				cout << " " << getCell(i, j) << " ";
			else
				cout << "   ";
		}
		cout << "|";
		cout << endl;
	}

	cout << " -";
	for (int j = 1; j <= BoardSize; j++)
		cout << "---";
	cout << "-";
	cout << endl;
}

bool board::fullySolved() {
	/*

	 This funtion checks if the board is fully solved based on whether or not there are any blank cells

	 */
	for (int i = 1; i < MaxValue + 1; i++) {		// loops through sudoku board
		for (int j = 1; j < MaxValue + 1; j++) {
			if (value[i][j] == Blank)		// checks if any of the cells is blank
				return false;		// if any of the cells in the board is blank, it returns false
		}
	}
	return true;		// else returns true
}

bool board::check(int i, int j, int value)
{
	if (checkConflictRow[i][value])
		return true;
	if (checkConflictCol[j][value])
		return true;
	if (checkConflictSq[squareNumber(i, j)][value])
		return true;
	return false;
}

//void board::solve()
//{
//	int blank_i;
//	int blank_j;
//	/*stack<int> possibilities;
//	stack<int> temp;*/
//	if (fullySolved()) {
//		print();
//		cout << "Backtrack Count: " << backtrack_count << endl;
//		exit(1);
//	}
//	findBlankCell(blank_i, blank_j);
//	/*find_compatible(blank_i, blank_j, possibilities);
//	while (!possibilities.empty()) {
//		temp.push(possibilities.top());
//		possibilities.pop();
//	}
//	while (!temp.empty()) {
//		setCell(blank_i, blank_j, temp.top());
//		updateConflicts();
//		print();
//		if (solve())
//			return true;
//		resetCell(blank_i, blank_j);
//		print();
//		updateConflicts();
//		temp.pop();
//	}*/
//	for (int k = 1; k <= 9; k++) {
//		if (check(blank_i, blank_j, k)) {
//			setCell(blank_i, blank_j, k);
//			updateConflicts();
//			print();
//			solve();
//			resetCell(blank_i, blank_j);
//			updateConflicts();
//		}
//	}
//}

void board::solve()
{
	int blank_i;
	int blank_j;
	if (fullySolved()) {
		print();
		cout << "Backtrack Count: " << backtrack_count << endl;
		return;
	}
	findBlankCell(blank_i, blank_j);
	for (int k = 1; k <= 9; k++) {
		if (!check(blank_i, blank_j, k)) {
			setCell(blank_i, blank_j, k);
			updateConflicts();
			solve();
			resetCell(blank_i, blank_j);
			updateConflicts();
		}
	}
}


void board::findBlankCell(int &row, int &col)
{
	for (row = 1; row <= BoardSize; row++) {
		for (col = 1; col <= BoardSize; col++) {
			if (isBlank(row, col)) 
				return;
		}
	}
}

void board::updateConflicts()
{
	setConflicts();
	setSqConflict();
}

void board::find_compatible(int row, int col, stack<int>& num)
{
	for (int i = 1; i <= 9; i++) {
		if (check(row, col, i))
			num.push(i);
	}
}

