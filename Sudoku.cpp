// Sudoku.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "board.h"

using namespace std;

int main()
{
	ifstream fin;
	string files[3] = { "sudoku1.txt", "sudoku2_new.txt", "sudoku3_new.txt" };		// all 3 filenames with sudoku puzzles
	int size = sizeof(files) / sizeof(*files);		// size of files array
	for (int i = 0; i < size; i++) {		// loops through files array to open each file one by one
		cout << "Sudoku Puzzle " << i + 1 << endl;
		fin.open(files[i].c_str());		// open file
		if (!fin)		// if file can't be opened, throw an error
		{
			cerr << "Cannot open " << files[i] << endl;
			exit(1);
		}
		try  
		{
			board b1(SquareSize);		// create board object 
			while (fin && fin.peek() != 'Z')		// loops through characters in file until the next character is Z
			{
				b1.initialize(fin);		// initializes the sudoku board from file input		
				//solved = b1.fullySolved();		// checks if board is fully solved (if no values are blank)
				//status = (solved) ? "Board solved!" : "Board not solved";		// conditional operator which sets status to either board solved or not solved based on if the board is solved
				//cout << endl << status << endl << endl;		// prints whether or not the board is solved
			}
			fin.close();		// closes file
			b1.print();		// prints the board
			b1.updateConflicts();
			//b1.printConflicts();		// prints all 3 conflicts boards
			b1.solve();
			//b1.printConflicts();
			b1.print();
			cout << "After return" << endl;
		}
		catch (indexRangeError &ex)
		{
			cout << ex.what() << endl;
			exit(1);
		}
	}
	return 0;
}
